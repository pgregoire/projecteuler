
import Data.Char

power :: Integer -> Integer -> Integer
power _ 0 = 1
power n p = n * (power n (p - 1))

sum' :: String -> Int
sum' [] = 0
sum' (x:xs) = (digitToInt x) + (sum' xs)

euler0016 :: Integer -> Int
euler0016 p = sum' (show (power 2 p))

main = do
        print (euler0016 15)
        print (euler0016 1000)
