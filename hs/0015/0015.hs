
fact :: Integer -> Integer
fact 1 = 1
fact n = n * (fact (n - 1))

power :: Integer -> Integer -> Integer
power _ 0 = 1
power n p = n * (power n (p - 1))

euler0015 n = (fact (2 * n)) `div` (power (fact n) 2)

main = do
        print (euler0015 2)
        print (euler0015 3)
        print (euler0015 4)
        print (euler0015 20)
