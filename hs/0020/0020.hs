
fact :: Integer -> Integer
fact 1 = 1
fact n = n * (fact (n - 1))

euler0020 n = sum [(read [x] :: Int) | x <- show (fact n)]

main = do
        print (euler0020 10)
        print (euler0020 100)
