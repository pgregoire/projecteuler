structure Main =
struct
	fun ndigits n =
		List.length (String.explode (LargeInt.toString n))


	fun euler0025 n =
	let
		fun f a b i =
		let
			val c = a + b
		in
			if n = (ndigits c)
			then i
			else f b c (i + 1)
		end
	in
		f 1 1 3
	end


	fun main (arg0, argv) =
	(
		print (Int.toString (euler0025 1000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0025", main)
end
