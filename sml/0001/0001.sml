structure Main =
struct
	fun range from to =
	(
		if (from = to) then
			[]
		else
			from :: range (from + 1) to
	)

	fun divides a b =
	(
		(0 = (b mod a))
	)

	fun div3 a =
	(
		divides 3 a
	)

	fun div5 a =
	(
		divides 5 a
	)

	fun prlst [] = ()
	  | prlst (x::xs) =
	(
		print (Int.toString(x) ^ "\n");
		prlst xs
	)

	fun isolate [] = []
	  | isolate (x::xs) =
	(
		x :: isolate (List.filter (fn y => y <> x) xs)
	)

	fun multiples to =
	(
		let
			val mul3 = List.filter div3 (range 1 to);
			val mul5 = List.filter div5 (range 1 to);
			val muls = mul3 @ mul5;
		in
			(*prlst (isolate muls);*)
			List.foldr op+ 0 (isolate muls)
		end
	)

	fun main (arg0, argv) =
	(
		print (Int.toString(multiples 1000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0001", main)
end
