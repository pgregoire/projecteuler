
structure Main =
struct
	fun fib n =
	(
		if 0 = n
		then 1
		else if 1 = n
		     then 2
                     else (fib (n - 2)) + (fib (n - 1))
	)

	fun fibs () =
	(
		let
			fun fibs' i acc =
				if List.null acc
				then fibs' 0 [(fib 0)]
				else if (List.last acc) > 4000000
				     then acc
				     else fibs' (i + 1) (acc @ [(fib (i + 1))])
		in
			fibs' 0 []
		end
	)

	fun euler0002 () =
	(
		let
			val f = fibs ()
			val e = List.filter (fn x => 0 = (x mod 2)) f
		in
			List.foldl (fn (a,v) => a + v) 0 e
		end
			
	)

	fun main (arg0, argv) =
	(
		print (Int.toString(euler0002 ()) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0002", main)
end
