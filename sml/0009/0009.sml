
structure Main =
struct
	fun power n 0 = 1
	  | power n p = n * (power n (p - 1))

	fun pyth a b c =
	(
		(power c 2) = (power a 2) + (power b 2)
	)

	fun euler0009 n =
	(
		let
			fun next a b c =
				if n = c
				then if n = b
				     then if n = a
					  then (0,0,0)
					  else (a+1,1,1)
				     else (a,b+1,1)
				else (a,b,c+1)
				
			fun euler0009' (a,b,c) =
			let
				val p = pyth a b c
				(*val _ = print ("p:" ^ Bool.toString(p) ^ "\n")*)
				(*val _ = print (Int.toString(a) ^" "^ Int.toString(b) ^" "^ Int.toString(c) ^" "^ "\n")*)
			in
				if p
				then if n = (a + b + c)
				     then if (a < b) andalso (b < c)
					  then (print (Int.toString(a) ^" "^ Int.toString(b) ^" "^ Int.toString(c) ^" "^ "\n"); (a * b * c))
					  else euler0009' (next a b c)
				     else euler0009' (next a b c)
				else euler0009' (next a b c)
			end
		in
			euler0009' (2,2,2)
		end
	)

	fun main (arg0, argv) =
	(
		(*print (Int.toString(euler0009 12) ^ "\n");*)
		print (Int.toString(euler0009 1000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0009", main)
end
