structure Main =
struct
	fun readfile path =
	let
		val ins  = TextIO.openIn path
		fun euler0022' copt acc =
			case copt of
			  NONE => (TextIO.closeIn ins; acc)
			| SOME c => euler0022' (TextIO.input1 ins) (acc @ [c])
	in
		String.implode (euler0022' (TextIO.input1 ins) [])
	end

	fun smallest l =
	let
		fun smallest' [] b = b
		  | smallest' (x::xs) b =
			let
				fun helper [] _ = b
				  | helper _ [] = x
				  | helper (y::ys) (z::zs) =
					if y > z
					then b
					else if y < z
					     then x
					     else helper ys zs

				val b' = helper (String.explode x) (String.explode b)
			in
				smallest' xs b'
			end
	in
		smallest' l "zzzzzzzzzzzzzz"
	end

	fun remove e [] = []
	  | remove e (x::xs) = if e = x then xs else [x] @ (remove e xs)

	fun sortw [] = []
	  | sortw xs =
		let
			val s = smallest xs
		in
			[s] @ (sortw (remove s xs))
		end

	

	fun indexof e l =
	let
		fun indexof' [] i = 0
		  | indexof' (x::xs) i =
			if e = x
			then i
			else indexof' xs (i + 1)
	in
		indexof' l 1
	end


	fun measure word =
	let
		val alpha = String.explode "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

		fun measure' [] = 0
		  | measure' (x::xs) = (indexof x alpha) + (measure' xs)
	in
		measure' (String.explode word)
	end


	fun euler0022 path =
	let
		val dat = readfile path
		val words = sortw (String.tokens (fn c => c = #",") dat)
		val cnt = List.length words

		fun euler0022' i =
		let
			val w = List.nth (words, i)
			val v = (measure w) * (i + 1)
		in
			if i = (cnt - 1)
			then v
			else v + (euler0022' (i + 1))
		end
	in
		euler0022' 0
	end


	fun main (arg0, argv) =
	(
		print (Int.toString (euler0022 "p022_names.txt") ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0022", main)
end
