(*
Let d(n) be defined as the sum of proper divisors of n (numbers
less than n which divide evenly into n). If d(a) = b and d(b) = a,
where a != b, then a and b are an amicable pair and each of a and
b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20,
22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of
284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.
*)

structure Main =
struct
	fun factorize n =
	let
		fun factorize' i acc =
			if (Real.fromInt i) > (Math.sqrt (Real.fromInt n))
			then acc
			else if (0 = (n mod i))
			     then factorize' (i + 1) ([i, (n div i)] @acc)
			     else factorize' (i + 1) acc
	in
		factorize' 2 [1]
	end

	fun sum xs = List.foldl (fn (a,b) => a + b) 0 xs

	fun isamicable n =
	let
		val a = sum (factorize n)
		val b = sum (factorize a)
	in
		(n = b) andalso (a <> b)
	end

	fun euler0021 to =
	let
		val t = (to - 1)
		val a = isamicable t
	in
		if 0 = t
		then 0
		else if isamicable t
		     then t + (euler0021 t)
		     else euler0021 t
	end

	fun main (arg0, argv) =
	(
		print (Bool.toString (isamicable 220) ^ "\n");
		print (Int.toString (euler0021 10000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0021", main)
end
