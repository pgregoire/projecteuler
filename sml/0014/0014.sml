(*
The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:
13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1

It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.
*)

structure Main =
struct
	fun colatz n =
	let
		fun colatz' x xs =
		let
			(*val _ = print ("C" ^ (LargeInt.toString x) ^ "\n");*)
		in
			if 1 = x
			then xs @ [x]
			else if 0 = (x mod 2)
			     then colatz' (x div 2) (xs @ [x])
			     else colatz' ((3 * x) + 1) (xs @ [x])
		end
	in
		colatz' (LargeInt.fromInt n) []
	end

	(*
	 * The longest Colatz chain up to n
	 *)
	fun euler0014 n =
	let
		fun euler0014' i bi bl =
		let
			(*val _ = print (">" ^ (Int.toString (i)) ^ "\n")
			val _ = print ("# (" ^ (Int.toString (bi)) ^ "," ^ (Int.toString (bl)) ^ ")\n")*)
		in
			if i > n
			then bi
			else let
				val l = List.length (colatz i)
			     in
				if l > bl
				then euler0014' (i + 1) i l
				else euler0014' (i + 1) bi bl
			     end

		end
	in
		euler0014' 2 0 0
	end

	fun main (arg0, argv) =
	(
		(*print ((Int.toString (euler0014 13)) ^ "\n");*)
		print ((Int.toString (euler0014 1000000)) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0014", main)
end
