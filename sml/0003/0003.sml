
structure Main =
struct
	fun biggestFactor n =
	(
		let
			fun bf' n' i c =
				if 1 = n'
				then c
				else if 0 = (LargeInt.mod (n',i))
				     then if i > c
					  then bf' (LargeInt.div (n',i)) 2 i
					  else bf' (LargeInt.div (n',i)) 2 c
				     else bf' n' (i + 1) c
		in
			bf' n 2 1
		end
	)

	fun euler0003 n =
	(
		let
			val f = biggestFactor n
		in
			if f = n
			then f
			else euler0003 f
		end
	)

	fun main (arg0, argv) =
	(
		print (LargeInt.toString(biggestFactor 7) ^ "\n");
		print (LargeInt.toString(biggestFactor 78) ^ "\n");
		print (LargeInt.toString(biggestFactor 77) ^ "\n");
		print (LargeInt.toString(biggestFactor 81) ^ "\n");
		print (LargeInt.toString(biggestFactor 13195) ^ "\n");
		print (LargeInt.toString(euler0003 13195) ^ "\n");
		print (LargeInt.toString(euler0003 600851475143) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0003", main)
end
