structure Main =
struct
	fun dayinmon m y =
		if 1 = m
		then if 0 = (y mod 400)
		     then 29
		     else if (0 = (y mod 4)) andalso (0 <> (y mod 100))
			  then 29
			  else 28
		else List.nth ([31,28,31,30,31,30,31,31,30,31,30,31], m)


	fun datediff (fd,fm,fy) (td,tm,ty) =
		if (fd = td) andalso (fm = tm) andalso (fy = ty)
		then 0
		else if fd = ((dayinmon fm fy) - 1)
		     then if 11 = fm
			  then 1 + datediff (0,0,(fy+1)) (td,tm,ty)
			  else 1 + datediff (0,(fm+1),fy) (td,tm,ty)
		     else 1 + datediff ((fd+1),fm,fy) (td,tm,ty)


	fun wdayat d m y =
	let
		val f = 0
	in
		(datediff (0,0,1900) (d,m,y)) mod 7
	end

	fun euler0019 from to =
	let
		fun nextmon m y =
			if 11 = m
			then (0, (y + 1))
			else ((m + 1), y)

		fun euler0019' (m,y) =
			if y > to
			then 0
			else if 6 = (wdayat 0 m y)
			     then 1 + euler0019' (nextmon m y)
			     else 0 + euler0019' (nextmon m y)
	in
		euler0019' (0,from)
	end


	fun main (arg0, argv) =
	(
		print (Int.toString(euler0019 1901 2000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0019", main)
end
