
structure Main =
struct
	fun isPrime n =
	(
		let
			fun isPrime' i =
				if i >= n
				then true
				else if 0 = (n mod i)
				     then false
				     else isPrime' (i + 1)
		in
			isPrime' 2
		end
	)

	fun primeAfter n =
	(
		let
			val _ = ""; (*print ("Checking after " ^ Int.toString(n) ^ "\n");*)
		in
			if isPrime (n + 1)
			then (n + 1)
			else primeAfter (n + 1)
		end
	)

	fun euler0010 n =
	(
		let
			(* sum primes below n *)
			fun euler0010' i s =
				let
					val p = primeAfter i
					val _ = print ("Got prime: " ^ Int.toString (p) ^ "\n");
				in
					if n < p
					then s
					else euler0010' (p + 1) (s + LargeInt.fromInt (p))
				end

		in
			euler0010' 2 2
		end
	)

	fun main (arg0, argv) =
	(
		print (LargeInt.toString(euler0010 10) ^ "\n");
		print (LargeInt.toString(euler0010 2000000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0010", main)
end
