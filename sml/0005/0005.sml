
structure Main =
struct
	fun euler0005 n =
	(
		let
			fun euler0005' i c =
				if 0 = i
				then c
				else if 0 <> (c mod i)
				     then euler0005' n (c + 1)
				     else euler0005' (i - 1) c
		in
			euler0005' n 1
		end
	)

	fun main (arg0, argv) =
	(
		print (Int.toString(euler0005 20) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0005", main)
end
