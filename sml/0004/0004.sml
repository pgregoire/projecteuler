
structure Main =
struct
	fun isPalindrome s =
	(
		let fun isPalindrome' l =
			if 1 = (List.length l) orelse 0 = (List.length l)
			then true
			else if (List.hd l) = (List.last l)
			     then isPalindrome' (List.drop (List.take (l, ((List.length l) - 1)), 1))
			     else false
		in
			isPalindrome' (String.explode s)
		end
	)

	fun euler0004 () =
	(
		let
			fun euler0004' b f1 f2 =
				let
					val _ = print (Int.toString (f1) ^ " " ^ Int.toString (f2) ^ "\n")
					val n = (f1 * f2)
					val p = isPalindrome (Int.toString n)
				in
					(* euler0004' n f1 (f2 - 1) *)
					if (f1 < 100) andalso (f2 < 100)
					then b
					else if f2 < 100
					     then euler0004' b (f1 - 1) 999
                                             else if p
						  then if n > b
						       then euler0004' n f1 f2
						       else euler0004' b f1 (f2 - 1)
						  else euler0004' b f1 (f2 - 1)
				end
		in
			euler0004' 0 999 999
		end
	)

	fun main (arg0, argv) =
	(
		print (Bool.toString(isPalindrome "a") ^ "\n");
		print (Bool.toString(isPalindrome "aa") ^ "\n");
		print (Bool.toString(isPalindrome "aba") ^ "\n");
		print (Bool.toString(isPalindrome "abba") ^ "\n");
		print (Bool.toString(isPalindrome "aabc") ^ "\n");
		print (Int.toString(euler0004 ()) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0004", main)
end
