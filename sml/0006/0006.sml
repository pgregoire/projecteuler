
structure Main =
struct
	fun power n 0 = 1
	  | power n p = n * power n (p - 1)

	fun sumSquares n =
	(
		let
			fun sumSquares' i s =
				if 0 = i
				then s
				else sumSquares' (i - 1) (s + power i 2 )
		in
			sumSquares' n 0
		end
	)

	fun squaresSum n =
	(
		let
			fun squaresSum' i s =
				if 0 = i
				then power s 2 
				else squaresSum' (i - 1) (s + i)
		in
			squaresSum' n 0
		end
	)

	fun euler0006 n =
	(
		(squaresSum n) - (sumSquares n)
	)

	fun main (arg0, argv) =
	(
		print (Int.toString(power 4 2) ^ "\n");
		print (Int.toString(squaresSum 10) ^ "\n");
		print (Int.toString(sumSquares 10) ^ "\n");
		print (Int.toString(euler0006 10) ^ "\n");
		print (Int.toString(euler0006 100) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0006", main)
end
