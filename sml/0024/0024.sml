(*
A permutation is an ordered arrangement of objects. For example, 3124
is one possible permutation of the digits 1, 2, 3 and 4. If all of
the permutations are listed numerically or alphabetically, we call it
lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2,
3, 4, 5, 6, 7, 8 and 9?
*)

structure Main =
struct
	fun swap i j l =
	let
		val i' = List.nth (l, i)
		val j' = List.nth (l, j)

		val h = List.take (l, i)
		val t = List.drop (l, (j + 1))

		val n = List.length l
		val m = List.drop (List.take (l, j), (i + 1))
	in
		h @ [j'] @ m @ [i'] @ t
	end


	(*
	 * largest index where a[i] < a[i + 1]
	 * swap
	 *)
	fun pandita l =
	let
		fun bk [] _ = ~1
		  | bk (x::[]) _ = ~1
		  | bk (x::y::xs) i =
			if x > y
			then i
			else bk (y::xs) (i + 1)

		val l' = List.rev l
		val k = bk l' 1
	in
		if 0 > k
		then []
		else	let
				fun bl [] _ b = b
				  | bl (x::xs) i b =
					if x > b
					then i
					else bl xs (i + 1) b

				val t = List.take (l', k)
				val l = bl t 0 (List.nth (l', k))
				val s = swap l k l'
				val f = List.rev (List.take (s, k)) @ List.drop (s, k)
			in
				List.rev f
			end
	end


	fun permute l =
	let
		fun f l' =
		let
			val n = pandita l'
		in
			if List.null n
			then []
			else [n] @ f n
		end
	in
		l :: (f l)
	end


	fun stril [] = ""
	  | stril (x::xs) = Int.toString(x) ^ (stril xs)


	fun euler0024 s n =
		stril (List.nth ((permute s), (n - 1)))


	fun main (arg0, argv) =
	(
		(*print ((euler0024 [1,2,3] 6) ^ "\n");*)
		print ((euler0024 [0,1,2,3,4,5,6,7,8,9] 1000000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0024", main)
end
