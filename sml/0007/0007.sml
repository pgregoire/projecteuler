
structure Main =
struct
	fun isPrime n =
	(
		let
			fun isPrime' i =
				if i >= n
				then true
				else if 0 = (n mod i)
				     then false
				     else isPrime' (i + 1)
		in
			isPrime' 2
		end
	)

	fun primeAfter n =
	(
		let
			val _ = ""; (*print ("Checking after " ^ Int.toString(n) ^ "\n");*)
		in
			if isPrime (n + 1)
			then (n + 1)
			else primeAfter (n + 1)
		end
	)

	fun euler0007 n =
	(
		let
			fun euler0007' i c =
				let
					val p = primeAfter c
					val _ = ""; (*print ("Got prime " ^ Int.toString(p) ^ "\n");*)
				in
					if i = n
					then p
					else euler0007' (i + 1) p
				end

		in
			euler0007' 1 1
		end
	)

	fun main (arg0, argv) =
	(
		print (Int.toString(euler0007 10001) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0007", main)
end
