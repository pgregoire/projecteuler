structure Main =
struct
	val pre20 = [
		"one", "two", "three", "four", "five", "six", "seven",
		"eight", "nine", "ten", "eleven", "twelve", "thirteen",
		"fourteen", "fifteen", "sixteen", "seventeen", "eighteen",
		"nineteen"
	]

	val tens = [
		"ten", "twenty", "thirty", "forty", "fifty", "sixty",
		"seventy", "eighty", "ninety"
	]

	(* no spaces or hyphens *)
	fun spell n =
		if 1000 < n
		then ""
		else if n = 1000
		     then "onethousand"
		     else if 0 < (n div 100)
			  then	let (* >= 100 *)
					val u = n div 100
					val r = n mod 100
				in
					if 0 = r
					then (spell u) ^ "hundred"
					else (spell u) ^ "hundredand" ^ (spell r)
				end
			  else	if 20 > n
				then List.nth (pre20, (n - 1))
				else	let
						val u = n div 10
						val r = n mod 10
						val s = List.nth (tens, (u - 1))
					in
						if 0 = r
						then s
						else s ^ (List.nth (pre20, ((n mod 10) - 1)))
					end

	fun euler0017 to =
	(
		let
			fun euler0017' i =
				if to < i
				then ""
				else (spell i) ^ (euler0017' (i + 1))
		in
			List.length (String.explode (euler0017' 1))
		end
	)

	fun main (arg0, argv) =
	(
		print ((spell 342) ^ "\n");
		print ((spell 115) ^ "\n");
		print ((spell 1000) ^ "\n");
		print (Int.toString(euler0017 342) ^ "\n");
		print (Int.toString(euler0017 115) ^ "\n");
		print (Int.toString(euler0017 1000) ^ "\n");
		OS.Process.success
	)

	val _ = SMLofNJ.exportFn("0017", main)
end
